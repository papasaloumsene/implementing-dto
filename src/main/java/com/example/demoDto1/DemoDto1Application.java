package com.example.demoDto1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoDto1Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoDto1Application.class, args);
	}

}

package com.example.demoDto1.controller;

import com.example.demoDto1.converter.StudentConverter;
import com.example.demoDto1.dto.StudentDto;
import com.example.demoDto1.entity.Student;
import com.example.demoDto1.exception.ResourceNotFoundException;
import com.example.demoDto1.repos.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class StudentController {

@Autowired
    private StudentRepository studentRepository;
@Autowired
    private StudentConverter converter;


@GetMapping("/find_all")
    public List<StudentDto> fetchAll(){

    List<Student> findAll = studentRepository.findAll();
    return converter.entityToDto(findAll);
}
@GetMapping("/find/{id}")
    public StudentDto findById(@PathVariable("id") Long id){

    Student st = studentRepository.findById(id).orElseThrow( ()-> new ResourceNotFoundException("the id" + id + "can not be found"));
    return converter.entityToDto(st);
}

@PostMapping("/save")
    public StudentDto save(@RequestBody StudentDto dto){

   Student student = converter.dtoToStudent(dto);
    student = studentRepository.save(student);

    return converter.entityToDto(student);
}
}

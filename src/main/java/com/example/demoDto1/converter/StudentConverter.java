package com.example.demoDto1.converter;

import com.example.demoDto1.dto.StudentDto;
import com.example.demoDto1.entity.Student;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class StudentConverter {

    public StudentDto entityToDto(Student student){

        StudentDto dto = new  StudentDto();
        dto.setId(student.getId());
        dto.setName(student.getName());
        dto.setPassword(student.getPassword());
        dto.setUsername(student.getUsername());
        return dto;
    }

    public List<StudentDto> entityToDto(List<Student> student){

        return student.stream()
                .map(x->entityToDto(x))
                .collect(Collectors.toList());
    }

    public Student dtoToStudent(StudentDto dto){
        Student student = new Student();
        student.setId(dto.getId());
        student.setName(dto.getName());
        student.setUsername(dto.getUsername());
        student.setPassword(dto.getPassword());

        return student;
    }

        public List<Student> dtoToStudent(List<StudentDto> dto){

            return dto.stream()
                    .map(x->dtoToStudent(x))
                    .collect(Collectors.toList());
        }

}
